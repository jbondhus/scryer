source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1'

gem 'jbuilder', '~> 2.0'

# Use Postgres as the database for Active Record
gem 'pg'
gem 'activerecord-import'

# Auth
gem 'devise'
gem 'devise-async'
gem 'jwt'

gem 'rails_12factor'

# Rate limiting
gem 'rack-attack'

# Memcached for caching
gem 'dalli'
gem 'connection_pool'

gem 'dogstatsd-ruby'

gem 'lograge' # We don't need huge multi-line messages...

# Monitoring
gem 'rollbar'
gem 'newrelic_rpm'

# Misc
gem 'hashids_rails', github: 'brianpetro/hashids_rails'

# Feature flagging
#gem 'rollout'
gem 'redis'

# Keen.IO
gem 'keen'
gem 'em-http-request', '~> 1.0'

# Easy running
gem 'foreman'

# Job queueing
gem 'sidekiq'
gem 'sidekiq-scheduler'
gem 'sinatra', :require => nil

# Use unicorn as the app server
gem 'unicorn'

# Sass/Bootstrap
gem 'sass-rails', '~> 5.0'
gem 'bootstrap-sass', '~> 3.3'
gem 'autoprefixer-rails'

# JS Libraries
gem 'jquery-rails'
gem 'bower-rails', '~> 0.9.1'
gem 'font-awesome-rails'
gem 'chosen-rails'
gem 'select2-rails'
gem 'rails-timeago', '~> 2.0'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.2'

# HAML support
gem 'haml-rails'
gem 'kaminari'

# ElasticSearch
gem 'elasticsearch'

# HTTP Client
gem 'faraday'
gem 'faraday_middleware', '~> 0.10.0'
gem 'rash'

# Fast String.empty?
gem 'fast_blank'

# Profiling
gem 'rack-mini-profiler'
gem 'flamegraph'
gem 'stackprof', '~> 0.2.7'

gem 'virtus'

# Rails5 removed some methods we needed.
gem 'record_tag_helper', '~> 1.0'

gem 'webpacker', '~> 3.5'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]

group :test do
  gem 'minitest-rails'
  gem 'minitest-reporters', '>= 0.5.0'
end

group :development do
  gem 'web-console'
  gem 'binding_of_caller' # better traces for better_errors
  gem 'spring'
  gem 'bullet'
  #gem 'quiet_assets'
  gem 'ruby-prof'
  gem 'annotate'
  gem 'letter_opener'
end
