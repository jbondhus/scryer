require 'test_helper'
require 'scryer/ffn/metadata_parser'

class MetadataParserTest < ActiveSupport::TestCase
  test 'handles_well_formed_input_1' do
    str = 'Rated: T - French - Chapters: 13 - Words: 69,618 - Reviews: 265 - Favs: 132 - Follows: 114 - Updated: - Published: - Harry P., Sirius B., Remus L.'

    expected = Scryer::FFN::Metadata.new(
        :characters => [
            {
                :name => 'Harry P.'
            },
            {
                :name => 'Sirius B.'
            },
            {
                :name => 'Remus L.'
            }
        ],
        :chapters => 13,
        :favs => 132,
        :follows => 114,
        :rated => 'T',
        :reviews => 265,
        :language => 'French',
        :words => 69618
    )

    actual = Scryer::FFN::MetadataParser.parse_info_string(str, [224])

    assert_equal expected, actual
  end
  test 'handles_well_formed_input_2' do
    str = 'Rated: M - English - Adventure/Romance - Chapters: 100 - Words: 1,404,198 - Reviews: 502 - Favs: 464 - Follows: 429 - Updated: - Published: - [Shepard (M), Miranda L.] Garrus V. - Complete'

    expected = Scryer::FFN::Metadata.new(
        :categories => [
            {
                :id => 6,
                :name => 'Adventure'
            },
            {
                :id => 2,
                :name => 'Romance'
            }
        ],
        :characters => [
            {
                :id => 66813,
                :name => 'Shepard (M)'
            },
            {
                :id => 37718,
                :name => 'Miranda L.'
            },
            {
                :id => 16082,
                :name => 'Garrus V.'
            },
        ],
        :chapters => 100,
        :favs => 464,
        :follows => 429,
        :rated => 'M',
        :relationships => [
            {
                :characters => [
                    {
                        :id => 66813,
                        :name => 'Shepard (M)'
                    },
                    {
                        :id => 37718,
                        :name => 'Miranda L.'
                    },
                ]
            }
        ],
        :reviews => 502,
        :status => 'Complete',
        :language => 'English',
        :words => 1404198
    )

    actual = Scryer::FFN::MetadataParser.parse_info_string(str, [2927])

    assert_equal expected, actual
  end
  test 'handles_story_without_categories_but_category_name_in_characters' do
    str = 'Rated: M - English - Chapters: 1 - Words: 33 - Published: - Darth Vader, Jango Fett, General Grievous, Darth Maul'

    expected = Scryer::FFN::Metadata.new(
        :categories => [],
        :characters => [
            {
                :id => 66813,
                :name => 'Darth Vader'
            },
            {
                :id => 37718,
                :name => 'Jango Fett'
            },
            {
                :id => 16082,
                :name => 'Darth Maul'
            },
        ],
        :chapters => 1,
        :favs => 0,
        :follows => 0,
        :rated => 'M',
        :relationships => [],
        :reviews => 0,
        :status => 'In-Progress',
        :language => 'English',
        :words => 33
    )

    actual = Scryer::FFN::MetadataParser.parse_info_string(str, [8])

    assert_equal expected, actual
  end
  test 'handles_story_with_id' do
    str = 'Rated: M - English - Chapters: 1 - Words: 33 - Published: - Darth Vader, Jango Fett, General Grievous, Darth Maul - id: 9704180 '

    expected = Scryer::FFN::Metadata.new(
        :categories => [],
        :characters => [
            {
                :id => 66813,
                :name => 'Darth Vader'
            },
            {
                :id => 37718,
                :name => 'Jango Fett'
            },
            {
                :id => 16082,
                :name => 'Darth Maul'
            },
        ],
        :chapters => 1,
        :favs => 0,
        :follows => 0,
        :rated => 'M',
        :relationships => [],
        :reviews => 0,
        :status => 'In-Progress',
        :language => 'English',
        :words => 33
    )

    actual = Scryer::FFN::MetadataParser.parse_info_string(str, [8])

    assert_equal expected, actual
  end
end