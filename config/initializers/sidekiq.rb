require 'scryer/active_record_uncached'

Sidekiq.configure_server do |config|
  config.server_middleware do |chain|
    chain.add Scryer::ActiveRecordUncached
  end
end