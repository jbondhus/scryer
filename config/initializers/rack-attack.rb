class Rack::Attack
  # Rack::Attack.safelist('allow from localhost') do |req|
  #   # Requests are allowed if the return value is truthy
  #   '127.0.0.1' == req.ip || '::1' == req.ip
  # end

  # Throttle story id lookups
  Rack::Attack.throttle('/stories/by_id', limit: 10, period: 1.minute) do |req|
    req.ip if req.path.starts_with? '/api/v1/stories'
  end

  # Throttle searches
  Rack::Attack.throttle('/search', limit: 25, period: 1.minute) do |req|
    req.ip if req.path.starts_with? '/search'
  end
end