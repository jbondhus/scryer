Rails.application.routes.draw do
  devise_for :users, :skip => [:sessions]
  as :user do
    get 'login' => 'devise/sessions#new', :as => :new_user_session
    post 'login' => 'devise/sessions#create', :as => :user_session
    delete 'logout' => 'devise/sessions#destroy', :as => :destroy_user_session
    post 'api/login' => 'api_authentication#create'
    get 'user/sso' => 'sso#create'
  end

  authenticate :user, lambda { |u| u.admin? } do
    require 'sidekiq/web'
    require 'sidekiq-scheduler/web'
    mount Sidekiq::Web => '/sidekiq'
  end


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  root 'search#index'
  get 'announcements/:id/hide', to: 'announcements#hide', as: 'hide_announcement'
  get 'author/:author_id' => 'author#index', :as => :author_by_id
  get 'faq' => 'pages#faq', :as => :faq
  get 'about' => 'pages#about', :as => :about
  get 'search' => 'search#search', :as => :search
  get 'search/id/:search_id' => 'search#search', :as => :search_by_id
  post 'search/click' => 'search#click', :as => :search_click
  post 'search/report_broken' => 'search#report_broken', :as => :search_report_broken
  # post 'saved_search' => 'search#created_saved_search', :as => :saved_searches
  get 'characters' => 'search#characters'
  get 'crossovers' => 'search#crossovers'
  get 'omni' => 'search#omni'
  post 'pensieve#bookmark' => 'pensieve#bookmark'
  match 'pensieve#bookmark' => 'pensieve#bookmark', :via => :options

  namespace :api do
    namespace :v1 do
      get 'stories/:id' => 'stories#show', :defaults => { :format => 'json' }
    end
  end

  get '/404', :to => 'errors#not_found'
  get '/422', :to => 'errors#unacceptable'
  get '/500', :to => 'errors#internal_error'

  namespace :user do
    get 'queue' => 'queue_entries#index', :as => :queue_entries
    post 'queue' => 'queue_entries#create'
    delete 'queue' => 'queue_entries#destroy'
    resources :saved_searches
    get 'recommendations' => 'recommendations#index', :as => :recommendations

    get 'hidden' => 'hidden#index'
    put 'hidden/author' => 'hidden#hide_author'
    delete 'hidden/author' => 'hidden#unhide_author'
    put 'hidden/story' => 'hidden#hide_story'
    delete 'hidden/story' => 'hidden#unhide_story'

  end
  resources :feedback

  get 'crawl/generate_assignments'
  get 'crawl/indexed_fandoms'
  post 'crawl/update_checkpoint'


  get 'dm', to: proc { [200, {}, ['']] }
  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
