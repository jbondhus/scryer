require 'faraday'

module Scryer
  class ForumClient
    def initialize(api_base='https://forums.darklordpotter.net', auth_token)
      @conn = Faraday.new(api_base, { ssl: { verify: false } }) do |faraday|
        # faraday.request  :json
        faraday.request  :url_encoded
        faraday.response :rashify
        faraday.response :json
        faraday.response :logger, ::Logger.new(STDOUT), bodies: true
        faraday.use :instrumentation
        faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
        faraday.headers['User-Agent'] = 'Scryer'
        faraday.headers['Authorization'] = "Bearer #{auth_token}"
      end
    end

    def reply(thread_id, post_body)
      body = {
          thread_id: thread_id,
          post_body: post_body
      }
      resp = @conn.post do |req|
        req.url 'api/?posts'
        req.body = body
      end

      resp.body
    end
  end
end
