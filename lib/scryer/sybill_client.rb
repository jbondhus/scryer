require 'faraday'

module Scryer
  class SybillClient
    def initialize(api_base='http://127.0.0.1:1777')
      @conn = Faraday.new(:url => api_base) do |faraday|
        faraday.request  :json
        faraday.response :rashify
        faraday.response :json
        faraday.response :logger, ::Logger.new(STDOUT), bodies: true
        faraday.use :instrumentation
        faraday.adapter  Faraday.default_adapter  # make requests with Net::HTTP
        faraday.headers['User-Agent'] = 'Scryer'
      end
    end

    def recommend(user_id)
      resp = @conn.get do |req|
        req.url "/recommend/stories/#{user_id}"
      end

      resp.body
    end
  end
end
