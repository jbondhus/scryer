module Scryer
  module Tagging
    module ContentTagger
      def self.tag(story, relationships = nil)
        tags = []

        relationships ||= story.relationships

        if story.summary =~ /multi\-pairings|(?:\/|x)\s?multi/i || relationships.any? { |r| r.size == 3 }
          if story.summary.count('/') >= 3
            tags << label(:harem)
          else
            tags << label(:multi)
          end
        end

        if story.summary =~ /harem/i || relationships.any? { |r| r.size > 3 }
          tags << label(:harem)
        end

        if story.summary =~ /(^|\s|\*|\(|\/|pre-)slash|mpreg|yaoi|m\/m|\bhp\/?dm|\bhphp\/?ss|\bhptr|\bdrarry/i && story.summary !~ /no(?:t)?(?: a)? slash/i
          tags << label(:slash, 'danger')
        end

        relationships.each { |group|
          genders = group.inject(Hash.new(0)) { |h, v| h[v.isfemale] += 1; h }

          tags << label(:slash, 'danger') if genders[false] > 1
          tags << label(:femslash, 'warning') if genders[true] > 1
        }

        if story.summary =~ /fem(me)?(\s|!)?slash/i
          tags << label(:femslash, 'warning')
        end

        tags << label(:gender_bender) if story.summary =~ /fem!|fem(?:\s)?harry/i

        tags.uniq
      end

      private

      def self.label(label, modifier='warning')
        [label, modifier]
      end
    end
  end
end
