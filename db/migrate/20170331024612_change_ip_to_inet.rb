class ChangeIpToInet < ActiveRecord::Migration[4.2]
  def change
    change_table :story_clicks do |t|
      t.change :ip, 'inet', using: 'ip::inet'
    end
  end
end
