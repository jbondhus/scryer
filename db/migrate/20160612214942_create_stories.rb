class CreateStories < ActiveRecord::Migration[4.2]
  def change
    create_table(:stories, :id => false) do |t|
      t.integer :id, :options => 'PRIMARY KEY'
      t.string :title, null: false
      t.references :author, null: false
      t.text :summary, null: false
      t.datetime :published, null: false
      t.datetime :updated, null: false
      t.datetime :last_seen, null: false
      t.string :status, null: false
      t.string :language, null: false
      t.string :rated, null: false
      t.integer :chapters, null: false
      t.integer :favs, null: false
      t.integer :follows, null: false
      t.integer :reviews, null: false
      t.integer :words, null: false
      t.integer :categories, null: false, :array => true
      t.jsonb :relationships, null: false
      t.integer :characters, null: false, :array => true
      t.integer :fandoms, null: false, :array => true

      t.primary_key :id
      t.index :author_id
      t.index :published
      t.index :updated
      t.index :words
      t.index :categories, using: 'gin'
      t.index :relationships, using: 'gin'
      t.index :characters, using: 'gin'
      t.index :fandoms, using: 'gin'

      t.timestamps null: false
    end
  end
end
