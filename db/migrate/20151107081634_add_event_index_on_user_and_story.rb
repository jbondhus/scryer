class AddEventIndexOnUserAndStory < ActiveRecord::Migration[4.2]
  def change
    add_index :pensieve_events, [:user_id, :story_id]
  end
end
