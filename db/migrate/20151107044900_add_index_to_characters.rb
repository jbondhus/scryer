class AddIndexToCharacters < ActiveRecord::Migration[4.2]
  def change
    add_index :characters, :fandom_id
  end
end
