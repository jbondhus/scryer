class CreateQueueEntries < ActiveRecord::Migration[4.2][5.1]
  def change
    create_table :queue_entries do |t|
      t.references :story
      t.belongs_to(:user, foreign_key: true)
      t.timestamps

      t.index ['user_id', 'story_id'], name: 'idx_queue_user_story', unique: true
    end
  end
end
