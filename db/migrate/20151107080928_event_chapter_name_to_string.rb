class EventChapterNameToString < ActiveRecord::Migration[4.2]
  def change
    change_column :pensieve_events, :chapter_name, :string

    execute "UPDATE pensieve_events SET chapter_id = chapter_name::integer WHERE event_name = 'ChapterLoadEvent'"
    execute "UPDATE pensieve_events SET chapter_name = null WHERE event_name = 'ChapterLoadEvent'"
  end
end
