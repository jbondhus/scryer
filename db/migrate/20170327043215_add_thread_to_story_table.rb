class AddThreadToStoryTable < ActiveRecord::Migration[4.2]
  def change
    create_table(:story_to_thread, :id => false) do |t|
      t.references :story
      t.integer :thread_id
      t.integer :vote_count
      t.integer :vote_total

      t.index [:story_id, :thread_id], unique: true
    end
  end
end
