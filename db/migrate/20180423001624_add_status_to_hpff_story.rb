class AddStatusToHpffStory < ActiveRecord::Migration[5.1]
  def change
    add_column :hpff_stories, :fetch_status, :string
  end
end
