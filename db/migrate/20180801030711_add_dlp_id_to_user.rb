class AddDlpIdToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :dlp_id, :integer
    add_column :users, :origin, :string, default: 'registration'
    add_index :users, :dlp_id
  end
end
