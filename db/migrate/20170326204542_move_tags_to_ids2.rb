class MoveTagsToIds2 < ActiveRecord::Migration[4.2]
  def change
    change_table :searches do |t|
      t.rename :tags_include_id, :tags_include_ids
      t.rename :tags_exclude_id, :tags_exclude_ids
    end
  end
end
