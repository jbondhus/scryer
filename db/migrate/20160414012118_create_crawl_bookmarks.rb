class CreateCrawlBookmarks < ActiveRecord::Migration[4.2]
  def change
    create_table :crawl_bookmarks, :id => false do |t|
      t.integer :fandoms, :array => true
      t.datetime :last_crawled
      t.boolean :bootstrapped

      t.timestamps null: false

      t.index :bootstrapped
    end

    execute 'ALTER TABLE crawl_bookmarks ADD PRIMARY KEY (fandoms)'
  end
end
