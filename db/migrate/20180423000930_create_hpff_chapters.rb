class CreateHpffChapters < ActiveRecord::Migration[5.1]
  def change
    create_table :hpff_chapters do |t|
      t.integer :story_id
      t.integer :chapter_id
      t.string :chapter_name
      t.string :chapter_blurb
      t.integer :wordcount

      t.timestamps
    end
  end
end
