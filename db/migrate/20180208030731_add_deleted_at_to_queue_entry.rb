class AddDeletedAtToQueueEntry < ActiveRecord::Migration[5.1]
  def change
    add_column :queue_entries, :deleted_at, :timestamp
  end
end
