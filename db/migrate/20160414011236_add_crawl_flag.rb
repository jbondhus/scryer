class AddCrawlFlag < ActiveRecord::Migration[4.2]
  def change
    add_column :fandoms, :is_indexed, :boolean, default: false
    add_index :fandoms, :is_indexed
  end
end
