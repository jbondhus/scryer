module SavedSearchesHelper
  def range(min, max)
    case
      when min && max
        "Between #{min} and #{max}"
      when min
        "At least #{min}"
      when max
        "At most #{max}"
      else
        ''
    end
  end
end
