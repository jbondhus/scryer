require 'pp'

module SearchHelper
  def story_pp(story)
    ''
  end

  def extract_names(vals, join_char=' ')
    vals.collect { |c| c.name }.join(join_char)
  end

  def show_characters(context, story)
    # categories.collect { |c| c.name }.join(join_char)

    unbound_characters = context.characters(story).collect { |c| c.name }

    relationships = context.relationships(story)
    bound_characters = relationships.flatten.collect { |c| c.name }

    # Remove characters that'll be formatted with pairing brackets
    unbound_characters.delete_if { |c| bound_characters.index(c) }

    # Colors for relationships
    colors = %w(primary danger warning success)

    formatted_relationships = relationships.collect do |relationship|
      color = colors.pop
      content_tag(:span, class: "text-#{color}") do
        [
          content_tag(:i, '', class: 'fa fa-margin fa-angle-left'),
          extract_names(relationship, ', '),
          content_tag(:i, '', class: 'fa fa-margin fa-angle-right')
        ].join.html_safe
      end.html_safe
    end

    (formatted_relationships + unbound_characters).join(', ').html_safe
  end

  def classify(context, story, &block)
    tags = []
    Rack::MiniProfiler.step('ContentTagger') do
      tags = Scryer::Tagging::ContentTagger.tag(story, context.relationships(story)).map do |(label, modifier)|
        content_tag(:span, label.to_s.gsub('_', ' '), class: "label label-#{modifier}")
      end
    end

    if tags.blank?
      ''
    else
      "#{tags.join(' ')} #{capture(&block)}".html_safe
    end
  end

  def has_results?
    @search_results.hits > 0 && @search_results.results.size > 0
  end

  def included_text(excluded, item_type)
    "#{excluded ? 'Excluded' : 'Included'} #{item_type}"
  end
end
