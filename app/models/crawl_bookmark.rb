require 'digest'

class CrawlBookmark < ActiveRecord::Base
  def self.bootstrapped
    where(bootstrapped: true).where('array_length(fandoms, 1) = 1')
  end

  def is_bootstrapped?
    bootstrapped
  end

  def should_crawl
    # If it's active or a top-level fandom, crawl. last_crawled is the "checkpoint," the last updated story, not the
    # last time the crawl executed.
    if last_crawled > 1.days.ago || fandoms.length  == 1 || should_recrawl
      return true
    end

    # Otherwise, spread load over 6h. We do this by taking a MD5 digest of the fandom ids and then taking that modulo
    # 6. When the hour of the day matches that value, we schedule.
    crawl_hour = Digest::MD5.hexdigest(fandoms.to_s).to_i(16) % 6
    crawl_hour == (Time.now.hour % 6)
  end

  def should_recrawl
    if needs_reindex
      return true
    end

    crawl_day = Digest::MD5.hexdigest(fandoms.to_s).to_i(16) % 28
    # 05:00 GMT = 11pm MDT.
    crawl_day == (Time.now.day % 28) && Time.now.hour == 5
  end
end
