require 'scryer/exceptions'

class SearchQuery
  def execute(user, search, from, size)
    if from+size > 1_000_000
      raise Exceptions::InvalidSearchError.new("Invalid page: #{from/size + 1}. Max page: #{1_000_000/size}")
    end

    popular_sort = { script: {
        lang: 'painless',
        inline: "_score +
        ((doc['meta.follows'].empty ? 0 : doc['meta.follows'].value) +
        (doc['meta.favs'].empty ? 0 : doc['meta.favs'].value) +
        (doc['meta.reviews'].empty ? 0 : doc['meta.reviews'].value))
          / Math.pow(
              Math.max(1,doc['meta.chapters'].value), 0.5)
          * (1/(((new Date()).getTime() - doc['updated'].value)/1000.0/86400/30))"
      }
    }

    long_and_recent = { script: {
        lang: 'painless',
        inline: "_score +
        Math.pow(
          (doc['meta.words'].empty ? 0 : doc['meta.words'].value), 0.5)
        * 1/(((new Date()).getTime() - doc['updated'].value)/1000.0/86400/30)"
      }
    }

    indexes = ['ffn_index']
    unless search.crossovers.empty?
      if search.include_source_fandoms
        indexes << 'ffncrossover_index'
      else
        indexes = ['ffncrossover_index']
      end

      search.crossovers = search.crossovers.delete_if { |f| f < 0 }
    end

    q = {
        query: {
            bool: {
                minimum_should_match: 0,

                must: must_clauses(search),
                must_not: must_not_clauses(user, search),
                should: should_clauses(search),
                filter: filter_clauses(search)
            }
        },
        size: size,
        from: from
    }

    if search.sort_by == '_popular'
      q[:query] = {
          function_score: {
              query: q[:query],
              script_score: popular_sort,
          }
      }
    elsif search.sort_by == '_long_recent'
      q[:query] = {
          function_score: {
              query: q[:query],
              script_score: long_and_recent,
          }
      }
    elsif search.sort_by == '_dlp'
      q[:sort] = {}
      q[:sort]['dlp_score'] = search.order_by
    else
      q[:sort] = {}
      q[:sort][search.sort_by] = search.order_by
    end

    response = {}
    Rack::MiniProfiler.step('ElasticSearch Query') do
      response = $elasticsearch.search index: indexes, body: q
    end

    result = {}
    if response != nil
      result['took'] = response['took']

      if response['hits'] != nil
        result['hits'] = response['hits']['total']
        result['max_score'] = response['hits']['max_score']
      end

      result['results'] = response['hits']['hits'].map do |hit|
        s = hit['_source']
        h = s
        h['story_id'] = h['story_id'].to_i

        # h['updated'] = Time.iso8601(h['updated'] + "Z")
        # h['published'] = Time.iso8601(h['published'] + "Z")
        # h['last_seen'] = Time.iso8601(h['last_seen'] + "Z")
        #
        # h['meta']['categories'] ||= []
        # h['meta']['characters'] ||= []
        # h['meta']['relationships'] ||= []
        # h['meta']['favs'] ||= 0
        # h['meta']['follows'] ||= 0
        # h['meta']['reviews'] ||= 0

        h
      end
    end

    result
  end

  def must_clauses(search)
    clauses = []

    if search.title != ''
      clauses << {
          query_string: {
              fields: ['title'],
              query: search.title.gsub(/\//, '\/')
          }
      }
    end
    if search.author != ''
      clauses << {
          query_string: {
              fields: ['author'],
              query: search.author.gsub(/\//, '\/')
          }
      }
    end
    if search.summary != ''
      clauses << {
          query_string: {
              fields: ['summary'],
              query: search.summary.gsub(/\//, '\/')
          }
      }
    end
    if search.status && search.status == 'hiatus'
      clauses << {
          query_string: {
              fields: ['summary'],
              query: search.status
          }
      }
    end


    unless search.category_optional.empty?
      unless search.category_optional_exclude
        clauses << or_clause(
            search.category_optional.map do |c|
              term 'meta.categories.id' => c
            end)
      end
    end

    unless search.character_optional.empty?
      unless search.character_optional_exclude
        clauses << or_clause(
            search.character_optional.map do |c|
              term 'meta.characters.id' => c
            end)
      end
    end

    clauses
  end

  def must_not_clauses(user, search)
    clauses = []

    clauses << (term 'deleted' => true)

    unless search.category_optional.empty?
      if search.category_optional_exclude
        clauses += search.category_optional.map { |c| term 'meta.categories.id' => c }
      end
    end


    unless search.character_optional.empty?
      if search.character_optional_exclude
        clauses += search.character_optional.map { |c| term 'meta.characters.id' => c }
      end
    end


    unless search.tags_exclude.empty?
      clauses += search.tags_exclude.map{ |t| term 'meta.tags' => t.storage_string }
    end

    unless search.relationships.empty?
      search.relationships.each do |ship|
        if ship.key?('exclude') && ship['characters']
          shipstring = ship['characters'].map(&:to_i).sort.join('_')

          if ship.key?('partial')
            clauses << (term 'shipstring_pairs' => shipstring)
          else
            clauses << (term 'shipstrings' => shipstring)
          end
        end
      end
    end

    unless user.nil?
      user_id = user.id

      clauses << (term 'hidden_by' => user_id)
      if search.read_filter == 'exclude_all_read'
        clauses << (term 'read_by.keyword' => "#{user_id}:0")
        clauses << (term 'read_by.keyword' => "#{user_id}:1")
      elsif search.read_filter == 'exclude_read_without_updates'
        clauses << (term 'read_by.keyword' => "#{user_id}:0")
      end
    end

    clauses
  end

  def should_clauses(search)
    []
  end

  def filter_clauses(search)
    clauses = []


    clauses << (terms 'fandoms.fandom_id' => search.fandoms)
    unless search.crossovers.empty?
      crossover_pairs = search.fandoms.map do |f|
        search.crossovers.map do |c|
          and_clause([
              (term 'fandoms.fandom_id' => f),
              (term 'fandoms.fandom_id' => c),
          ])
        end
      end.flatten

      clause = or_clause(crossover_pairs)
          #and_clause((search.fandoms + search.crossovers).map{|f| term 'fandoms.fandom_id' => f})
      if search.include_source_fandoms
        # only apply filter to ffn_crossover_index
        clause = or_clause([
          {
             indices: {
                 indices: ['ffn_index'],
                 query: (terms 'fandoms.fandom_id' => search.fandoms),
                 no_match_query: 'none'
             }
          },
          {
            indices: {
                indices: ['ffncrossover_index'],
                query: clause,
                no_match_query: 'none'
            }
          }
        ])

        # clause = {
        #     indices: {
        #         indices: ['ffncrossover_index'],
        #         query: clause,
        #         no_match_query: 'none'
        #     }
        #   }
      end
      clauses << clause
    end

    unless search.tags_include.empty?
      clauses << (terms 'meta.tags' => search.tags_include.map{|t| t.storage_string})
    end

    unless search.category_required.empty?
      search.category_required.map do |c|
        clauses << (term 'meta.categories.id' => c)
      end
    end

    unless search.character_required.empty?
      search.character_required.map do |c|
        clauses << (term 'meta.characters.id' => c)
      end
    end

    unless search.relationships.empty?
      subclauses = []
      search.relationships.each do |ship|
        unless ship.key?('exclude')
          if ship['characters']
            shipstring = ship['characters'].map(&:to_i).sort.join('_')
            if ship.key?('partial')
              subclauses << (term 'shipstring_pairs' => shipstring)
            else
              subclauses << (term 'shipstrings' => shipstring)
            end
          end
        end
      end
      clauses << or_clause(subclauses)
    end

    clauses << (terms 'meta.rated' => search.rating)

    if search.wordcount_lower
      clauses << (range 'meta.words', :gte, search.wordcount_lower)
    end
    if search.wordcount_upper
      clauses << (range 'meta.words', :lte, search.wordcount_upper)
    end
    if search.chapters_lower
      clauses << (range 'meta.chapters', :gte, search.chapters_lower)
    end

    if search.chapters_upper
      clauses << (range 'meta.chapters', :lte, search.chapters_upper)
    end

    if search.published_before
      clauses << (range 'published', :lte, search.published_before)
    end

    if search.published_after
      clauses << (range 'published', :gte, search.published_after)
    end

    if search.updated_after
      clauses << (range 'updated', :gte, search.updated_after)
    end

    if search.updated_before
      clauses << (range 'updated', :lte, search.updated_before)
    end

    if search.status && search.status != 'hiatus' && search.status != ''
      clauses << (term 'meta.status' => search.status)
    end

    if search.language
      clauses << (term 'meta.language' => search.language)
    end

    if search.sort_by == '_dlp'
      clauses << (terms '_id' => StoryToThread.all.pluck(:story_id).to_a)
    end

    clauses
  end

  def term(field_thing)
    { term: field_thing }
  end

  def terms(field_thing)
    { terms: field_thing }
  end

  def or_clause(clauses, min_match = 1)
    {
        bool: {
            minimum_should_match: min_match,
            should: clauses
        }
    }
  end

  def and_clause(clauses)
    {
        bool: {
            must: clauses
        }
    }
  end

  def range(field, bound_type, value)
    c = {
        range: {

        }
    }
    c[:range][field] = {}
    c[:range][field][bound_type] = value
    c
  end

  def to_s
    pretty_inspect
  end
end
