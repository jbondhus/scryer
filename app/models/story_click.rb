class StoryClick < ActiveRecord::Base
  after_save :reindex_story

  belongs_to :story
  belongs_to :story_update
  belongs_to :user

  def updated
    if story_update.nil?
      story.updated
    else
      story_update.updated
    end
  end

  def reindex_story
    unless user_id.nil?
      story.reindex
    end
  end
end
