# == Schema Information
#
# Table name: story_update
#
#  id             :integer          not null, primary key
#  story_id       :bigint
#  story_title    :text
#  author_id      :bigint
#  author_name    :text
#  fandoms        :integer[]
#  update_contents :jsonb
#  crawled_at     :datetime
#  updated_at     :datetime
#

class StoryUpdate < ActiveRecord::Base
  belongs_to :story

  def summary
    update_contents['summary']
  end

  def updated
    DateTime.parse(update_contents['updated'])
  end

  # Returns previous updates in descending order
  def previous_updates
    StoryUpdate.where('story_id = ? AND id < ?', self.story_id, self.id).order(id: :desc)
  end
end
