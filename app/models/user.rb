require 'securerandom'
class User < ActiveRecord::Base
  before_create :set_auth_token

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :username, :presence => true, :uniqueness => true, length: 2..64

  has_many :feedbacks
  has_many :favorites
  has_many :hidden_authors
  has_many :hidden_stories
  has_many :saved_searches
  has_many :story_clicks
  has_many :pensieve_events
  has_many :queue_entries

  def to_s
    username
  end

  def admin?
    id == 1
  end

private
  def set_auth_token
    return if auth_token.present?
    self.auth_token = generate_auth_token
  end

  def generate_auth_token
    SecureRandom.uuid.gsub(/\-/,'')
  end
end
