# == Schema Information
#
# Table name: characters
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#  fandom_id  :integer
#

class Character < ActiveRecord::Base
  belongs_to :fandom

  def gender
    if isfemale.nil?
      return :unknown
    end

    if isfemale?
      :female
    else
      :male
    end
  end

  def self.for_fandoms(fandoms)
    if fandoms && fandoms.size > 1
      if fandoms.include?(-1)
        fandoms = fandoms.keep_if{|f| f > 0}
        fandoms += Fandom.fandom_facets(fandoms).map(&:id).keep_if{|f| f > 0}
      end
      #
      characters = Character.where(:fandom_id => fandoms).joins(:fandom).includes(:fandom).limit(10000)
      characters.collect do |c|
        name = "#{c.name } (#{c.fandom.name})"
        c = Character.new(id: c.id, name:name, fandom_id: c.fandom_id)
        c.readonly!
        c
      end.sort_by{|c| c.name}
    elsif fandoms && fandoms.size == 1
      where(:fandom_id => fandoms.first).order('name ASC')
    else
      []
    end
  end
end
