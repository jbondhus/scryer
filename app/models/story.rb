require 'scryer/tagging/content_tagger'

class Story < ActiveRecord::Base
  belongs_to :author
  has_many :hidden_authors
  has_many :hidden_stories
  has_one :story_to_thread
  has_many :story_clicks, -> { where('user_id IS NOT NULL') }

  def self.reindex_all
    Story.all.find_in_batches(start: 0, batch_size: 20) { |g| g.each(&:reindex) }
  end

  def deleted?
    !deleted_at.nil?
  end

  def reindex
    ReindexStoryWorker.perform_async(id)
  end

  def crossover?
    fandom_ids.size > 1
  end

  def category_ids
    self[:categories]
  end

  def categories
    Category.where(:id => self[:categories])
  end

  def character_ids
    self[:characters]
  end

  def characters
    Character.where(:id => self[:characters])
  end

  def fandom_ids
    self[:fandoms]
  end

  def fandoms
    Fandom.where(:id => self[:fandoms])
  end

  def relationships_raw
    self[:relationships]
  end

  def relationships
    if @relationships_cache
      return @relationships_cache
    end

    relations = self[:relationships]
    unless relations && relations.size > 0
      return []
    end

    if !relations[0].is_a?(Array) && relations[0].has_key?('characters')
      relations = relations.map { |g| g['characters'].map { |c| { 'character_id' => c['id'] } } }
    end

    character_ids = relations.flat_map { |group| group.map { |character| character['character_id'].to_i } }
    characters = Character.where(:id => character_ids).to_a.index_by(&:id)

    @relationships_cache = relations.map do |group|
      group.map { |character| character['character_id'].to_i }
           .map { |id| characters[id] }
    end || []

    @relationships_cache
  end

  def url_slug
    title.gsub(/[^a-z0-9\s]/i, '').gsub(/\s+/, '-')
  end

  def url
    "https://www.fanfiction.net/s/#{id}/1/#{url_slug}"
  end

  def tags
    Scryer::Tagging::ContentTagger.tag(self).map do |tag|
      Tag.new(name: tag[0], system: true)
    end
  end

  ## update method
  ## updates story row, adds story update if changed, indexes into ES
  def self.update(doc)
    author = Author.find_or_initialize_by(id: doc['author_id'].to_i)

    if author.new_record?
      name = doc['author']

      if name == '' || name.nil?
        warn "Warn: author(#{s['author_id']}) name is empty or nil: #{name}"
      else

        author.name = name
        author.save!
      end
    end
    orig = Story.where(id: doc['story_id']).take
    story = from_es_doc(author, doc)
    story.save!

    if story_updated?(orig, story)
      update = StoryUpdate.create!(
                     story_id: story.id,
                     story_title: story.title,
                     author_id: author.id,
                     author_name: author.name,
                     fandoms: story.fandoms,
                     update_contents: doc)

      # Don't notify DLP in cases where we're backfilling a fandom and find an update we didn't see before.
      if story.updated > 1.week.ago
        notify_dlp(update)
      end
    end

    story
  end

  def self.story_updated?(orig, story)
    !orig || story.updated > orig.updated
  end

  def self.notify_dlp(update)
    StoryToThread.where(:story_id => update.story_id).each do |dlp_thread|
      DlpThreadNotificationWorker.perform_async(update.id, dlp_thread.thread_id)
    end
  end

  def self.load_from_es(index = 'ffn_index')
    # Open the "view" of the index
    response = $elasticsearch.search index: index, search_type: 'scan', scroll: '5m', size: 100, body: {
        query: {
            bool: {
                must: [],
                must_not: [],
                should: []
            }
        }
    }

    # Call `scroll` until results are empty
    while response = $elasticsearch.scroll(scroll_id: response['_scroll_id'], scroll: '5m') and not response['hits']['hits'].empty? do
      # puts response['hits']['hits'].map { |r| r['_source'] }

      response['hits']['hits'].each { |r|
        s = r['_source']

        author = Author.find_or_initialize_by(id: s['author_id'].to_i)

        if author.new_record?
          name = s['author']

          if name == '' || name.nil?
            warn "Warn: author(#{s['author_id']}) name is empty or nil: #{name}"
          else

            author.name = name
            author.save!
          end
        end

        begin
          from_es_doc(author, s).save!
        rescue ArgumentError => e
          warn "Failed to save story: #{s['story_id']}: #{e}"
        rescue ActiveRecord::RecordNotUnique
          # ignored
        rescue ActiveRecord::StatementInvalid => e
          warn "Failed to save story: #{s['story_id']}: #{e}"
        end
      }

    end
  end

  def self.from_es_doc(author, s)
    story = Story.find_or_initialize_by(id: s['story_id'])

    story.assign_attributes(
        id: s['story_id'],
        title: s['title'],
        author_id: author.id,
        summary: s['summary'],
        published: DateTime.iso8601(s['published']),
        updated: DateTime.iso8601(s['updated']),
        last_seen: DateTime.iso8601(s['last_seen']),
        status: s['meta']['status'],
        rated: s['meta']['rated'],
        chapters: s['meta']['chapters'],
        language: (s['meta']['language']||'English'),
        reviews: s['meta']['reviews'].to_i,
        follows: s['meta']['follows'].to_i,
        favs: s['meta']['favs'].to_i,
        words: s['meta']['words'].to_i,
        categories: (s['meta']['categories']||[]).map { |c| c['id'].to_i },
        characters: (s['meta']['characters']||[]).map { |c| c['id'].to_i },
        relationships: (s['meta']['relationships']||[]),
        fandoms: s['fandoms'].map { |f| f['fandom_id'].to_i },
    )

    story
  end

  def to_es_doc
    {
        story_id: id,
        title: title,
        author_id: author.id,
        author: author.name,
        summary: summary,
        published: published,
        updated: updated,
        meta: {
            status: status,
            rated: rated,
            chapters: chapters,
            language: language,
            reviews: reviews,
            follows: follows,
            favs: favs,
            words: words,
            categories: Category.where(:id => categories).map {|c| { id: c.id, name: c.name }},
            characters: Character.where(:id => characters).map {|c| { id: c.id, name: c.name }},
            relationships: relationships,
        },
        fandoms: Fandom.where(:id => fandoms).map {|f| { fandom_id: f.id, fandom_name: f.name }},
    }.deep_stringify_keys
  end

  def es_index
    crossover? ? 'ffncrossover_index' : 'ffn_index'
  end
end
