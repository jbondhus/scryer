class Author < ActiveRecord::Base
  has_many :stories

  def url_slug
    name.gsub(/[^a-z0-9\s]/i, '').gsub(/\s+/, '-')
  end

  def url
    "https://www.fanfiction.net/u/#{id}/#{url_slug}"
  end
end
