class Tag < ActiveRecord::Base
  def storage_string
    prefix = system? ? 's' : 'u'

    "#{prefix}:#{name.downcase.gsub(/[^a-z-]/, '_')}"
  end
end
