# == Schema Information
#
# Table name: fandoms
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

require 'hashie/mash'

class Fandom < ActiveRecord::Base
  belongs_to :fandom_category, foreign_key: 'category_id'
  has_many :characters

  def category_slug
    unless fandom_category.nil?
      fandom_category.slug
    end
  end

  def self.fandom_facets(fandom_id)
    if fandom_id.nil? || fandom_id.none?
      return []
    end

    if fandom_id.is_a?(Array)
      fandom_id = fandom_id.first
    end

    Rails.cache.fetch("fandom_facets_#{fandom_id}", :expires_in => 6.hours) do
      buckets = $elasticsearch.search(
          index: 'ffncrossover_index',
          body: {
            aggregations: {
              fandoms: {
                filter: { term: { 'fandoms.fandom_id' => fandom_id }},
                aggregations: {
                  fandoms: {
                    terms: {
                      size: 1000,
                      field: 'fandoms.fandom_id'
                    }
                  }
                }
              }
            }
          })['aggregations']['fandoms']['fandoms']['buckets']

      fandom_sizes = buckets.inject({}) do |r,a|
        r[a['key']] = a['doc_count']
        r
      end

      ids_to_lookup = fandom_sizes.select { |id, count| count > 3 }.keys
      facets = Fandom.where(:id => ids_to_lookup).map do |f|
        story_count = fandom_sizes[f.id] || 0
        { id: f.id, name: f.name + " (#{story_count})", stories: story_count }
      end.sort_by do |f|
        f[:stories]
      end.reverse.keep_if do |f|
        f[:id] != fandom_id.to_i && f[:stories] > 0 # remove the current fandom from the list
      end.map do |f|
        Hashie::Mash.new f
      end

      all_crossover_count = facets.map{|f| f[:stories]}.sum

      [Hashie::Mash.new({id: -1, name: "All Crossovers (#{all_crossover_count})", stories: all_crossover_count})] + facets
    end
  rescue Exception => e
    Rollbar.warn("Fandom: Failed to pull facets: #{e}")
    Rails.logger.warn("Fandom: Failed to pull facets: #{e}")
    Fandom.all.order(:name)
  end

  def self.name_sentence(fandoms)
    Rails.cache.fetch("fandom_names_#{fandoms}", :expires_in => 6.hours) do
      where(:id => fandoms).order(:id).pluck(:name).to_sentence
    end
  end

  def self.indexed_fandoms
    Rails.cache.fetch("indexed_fandoms", :expires_in => 10.minutes) do
      bootstrapped_fandoms = CrawlBookmark
                                 .bootstrapped
                                 .pluck(:fandoms)
                                 .flatten

      criteria = where(:is_indexed => true)
      unless bootstrapped_fandoms.empty?
        criteria = criteria.where(:id => bootstrapped_fandoms)
      end

      criteria.order('name ASC')
    end
  end
end
