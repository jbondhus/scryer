class HiddenAuthor < ApplicationRecord
  after_save :reindex_author_stories
  after_destroy :reindex_author_stories

  belongs_to :user
  belongs_to :author

  private
  def reindex_author_stories
    author.stories.each(&:reindex)
  end
end
