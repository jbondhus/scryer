class IndexCharacterWorker
  include Sidekiq::Worker
  sidekiq_options :backtrace => true

  def perform(character_id, body)
    logger.info "[#{character_id}] Indexing character from Crawler"

    if character_id.to_i == 0
      logger.info "[#{character_id}] Skipping 'All Characters' character."
      return
    end

    character = Character.where(:id => character_id).first

    name = body['name']
    fandom_id = body['fandom_id'].to_i

    if character.nil?
      character = Character.create(:id => character_id, :fandom_id => fandom_id)
    end

    if (!name.nil? && character.name != name) || character.fandom_id != fandom_id
      character.name = name
      character.fandom_id = fandom_id
      character.save
    end
  end
end
