require 'pp'

class BrokenLinkWorker
  include Sidekiq::Worker
  sidekiq_options :backtrace => true

  def perform(story_id, is_crossover = false)
    url = "https://www.fanfiction.net/s/#{story_id}/1/"

    logger.info "[#{story_id}] Checking #{url} for a broken link..."

    response = Faraday.get(url) do |req|
      req.headers['User-Agent'] =
          'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36'
    end

    if response.body.include?('Unable to locate story. Code 1')
      logger.info "[#{story_id}] FFN response included 'Code 1', marking as deleted."

      story = Story.find_by(id: story_id)
      if story.nil?
        index = is_crossover ? 'ffn_index_crossover' : 'ffn_index'

        es_doc = $elasticsearch.get index: index, id: story_id, type: 'story'


        src = es_doc['_source']
        src['deleted'] = true

        $elasticsearch.index index: index, type: es_doc['_type'], id: story_id, body: src
      else
        story.deleted_at = DateTime.now
        story.save
        story.reindex
      end
    else
      logger.info "[#{story_id}] Found story at #{url}, skip marking as deleted."
    end

    # Reindex just in case other things are wrong
    Story.find(story_id).reindex
  rescue Elasticsearch::Transport::Transport::Errors::NotFound
    logger.info "[#{story_id}] Cannot find document in ES, skip marking as deleted."
  end
end
