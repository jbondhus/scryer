require 'pp'
require 'nokogiri'

class ImportFavoritesWorker
  include Sidekiq::Worker
  sidekiq_options :backtrace => true

  def perform(user_id, ffn_user_id)
    url = "https://www.fanfiction.net/u/#{ffn_user_id}/1/"

    logger.info "[#{user_id}] Importing favorites from #{url}..."

    response = Faraday.get(url) do |req|
      req.headers['User-Agent'] =
          'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2704.84 Safari/537.36'
    end

    document = Nokogiri::HTML(response.body)

    favorite_stories = document.css('.favstories')
    if favorite_stories
      story_ids = favorite_stories.map { |s| [user_id, s.attr('data-storyid').to_i] }

      logger.info "[#{user_id}] Found #{story_ids.size} favorites."

      Favorite.import(
        [:user_id, :story_id],
        story_ids,
        on_duplicate_key_update: {
            columns: [:updated_at],
            conflict_target: [:user_id, :story_id]
        },
        validate: false
      )

      logger.info "[#{user_id}] Done importing favorites."
    end
  end
end
