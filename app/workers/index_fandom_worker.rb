class IndexFandomWorker
  include Sidekiq::Worker
  sidekiq_options :backtrace => true

  def perform(fandom_id, body)
    logger.info "[#{fandom_id}] Indexing fandom from Crawler"

    if fandom_id.to_i == 0
      logger.info "[#{fandom_id}] Skipping fandom."
      return
    end

    fandom = Fandom.where(:id => fandom_id).first

    name = body['name']

    if fandom.nil?
      fandom = Fandom.create(:id => fandom_id, :name => name)
    end

    if !name.nil? && fandom.name != name && name.strip != ""
      fandom.name = name.strip
      fandom.save
    end
  end
end
