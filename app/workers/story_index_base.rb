require 'scryer/tagging/content_tagger'
require 'scryer/ffn/metadata_parser'

class StoryIndexBase
  include Sidekiq::Worker

  def preprocess(document)
    # Parse infostring if available
    if document.meta.info_string
      fandom_ids = document.fandoms.map(&:fandom_id)
      metadata = Scryer::FFN::MetadataParser.parse_info_string(document.meta.info_string, fandom_ids)
      document.meta = metadata
    end

    # Turn into AR record
    # This is ugly, fix it up.
    author = Author.find_or_initialize_by(id: document['author_id'].to_i)
    story = Story.from_es_doc(author, document)

    # Content Tags
    document.meta.tags = Scryer::Tagging::ContentTagger.tag(story).map { |tag| "s:#{tag[0]}" }

    # DLP Scores
    unless (thread = StoryToThread.find_by(:story_id => document.story_id)).nil?
      document.dlp_score = thread.score
    end

    document
  end

  def denormalize(story, document)
    # Denormalize read state
    read_by = story.story_clicks.inject({}) do |map, click|
      if map[click.user.id] == nil || map[click.user.id] < click.updated
        map[click.user.id] = click.updated
      end

      map
    end

    hidden_by = Set.new
    HiddenAuthor.where(:author_id => story.author.id).each { |h| hidden_by.add(h.user.id) }
    HiddenStory.where(:story_id=> story.id).each { |h| hidden_by.add(h.user.id) }
    document.hidden_by = hidden_by

    document.read_by = read_by.map do |id,last_click|
      has_updates = last_click < story.updated

      "#{id}:#{has_updates ? 1 : 0}"
    end

    document
  end

  def index(story_id, document)
    if document.nil?
      # Empty.
      return
    end

    document.delete('_id')
    document = Hashie::Rash.new(document)
    document = preprocess(document)

    story = Story.update(document)

    # Denormalize read/hidden stories.
    document = denormalize(story, document)

    # Ships for searchin'
    document.shipstrings = story.relationships.map do |group|
      group.map(&:id).sort.join('_')
    end

    # The cross product of each pairing such that [A, B, C, D] you can search [A, B] or [A, B, C, D]
    document.shipstring_pairs = story.relationships.flat_map do |group|
      ids = group.map(&:id).sort.to_a

      (1..4).flat_map do |size|
        ids.combination(size).map { |g| g.join('_') }
      end.reject(&:empty?)
    end

    $elasticsearch_index.index index: story.es_index, type: 'story', id: story_id, body: document
  end
end
