class DlpThreadNotificationWorker
  include Sidekiq::Worker
  sidekiq_options :backtrace => true

  def perform(story_update_id, dlp_thread_id)
    logger.info "DlpThreadNotifier(Story(#{story_update_id}), Thread(#{dlp_thread_id})): Notifying thread."

    update = StoryUpdate.find(story_update_id)
    email_body = ApplicationController.render(
        'dlp_posts/story_update',
        assigns: {
            story: update.story,
            update: update
        }
    )

    $forums.reply(dlp_thread_id, email_body)
  end
end
