class User::RecommendationsController < ApplicationController
  before_action :authenticate_user!

  def index
    top_n = $sybill.recommend(@current_user.id).map { |recommendation| recommendation['story_id'].to_i }

    @story_context = StoryFetchContext.new(current_user, top_n)
    @stories = @story_context.stories
  end

  def create
    @queue_entry = @current_user.queue_entries.where(:story_id => params[:story_id]).first
    @queue_entry ||= @current_user.queue_entries.build

    if @queue_entry.new_record? || !@queue_entry.deleted_at.nil?
      @queue_entry.deleted_at = nil
      @queue_entry.story_id = params[:story_id]
      @queue_entry.save!
      render json: @queue_entry, status: :created
    else
      render json: @queue_entry, status: :conflict
    end
  end

  def show

  end

  def destroy
    @current_user.queue_entries.where(:story_id => params[:story_id]).each(&:soft_delete)

    render status: :ok, body: nil
  end
end
