class User::QueueEntriesController < ApplicationController
  before_action :authenticate_user!

  def index
    @queue_entries = @current_user.queue_entries
                         .active
                         .joins(:story)
                         .includes(story: [:author, :story_to_thread])
                         .to_a
    story_ids = @queue_entries.map(&:story_id)

    @story_context = StoryFetchContext.new(current_user, story_ids)
  end

  def create
    @queue_entry = @current_user.queue_entries.where(:story_id => params[:story_id]).first
    @queue_entry ||= @current_user.queue_entries.build

    if @queue_entry.new_record? || !@queue_entry.deleted_at.nil?
      @queue_entry.deleted_at = nil
      @queue_entry.story_id = params[:story_id]
      @queue_entry.save!
      render json: @queue_entry, status: :created
    else
      render json: @queue_entry, status: :conflict
    end
  end

  def show

  end

  def destroy
    @current_user.queue_entries.where(:story_id => params[:story_id]).each(&:soft_delete)

    render status: :ok, body: nil
  end
end
