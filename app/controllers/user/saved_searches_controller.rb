class User::SavedSearchesController < ApplicationController
  before_action :authenticate_user!, :except => [:show]
  before_action :set_saved_search, only: [:show, :edit, :update, :destroy]

  # GET /saved_searches
  # GET /saved_searches.json
  def index
    @saved_searches = @current_user.saved_searches.joins(:search)
  end

  # GET /saved_searches/1
  # GET /saved_searches/1.json
  def show
    if @saved_search
      respond_to do |format|
        format.html { redirect_to search_path, search_id: @saved_search.id }
        format.json { render json: { :saved => @saved_search, :search=> @saved_search.search } }
      end
    else
      respond_to do |format|
        format.html { render status: 404 }
        format.json { render status: 404 }
      end
    end
  end

  # GET /saved_searches/new
  def new
    @saved_search = SavedSearch.new
  end

  # GET /saved_searches/1/edit
  def edit
  end

  # POST /saved_searches
  # POST /saved_searches.json
  def create
    puts saved_search_params
    id = SavedSearch.dehash_id(saved_search_params['id'])
    @saved_search = SavedSearch.where(:id => id, :user => @current_user).includes(:search).first
    @saved_search ||= @current_user.saved_searches.build

    if @saved_search.new_record?
      @search = Search.new(search_params)
      @saved_search = current_user.saved_searches.build(saved_search_params)
      # @search.save!
      @saved_search.search = @search
    else
      # not a new record
      @saved_search.search.update(search_params)
      @saved_search.touch
    end

    respond_to do |format|
      if @saved_search.save
        format.html { redirect_to [:user, @saved_search], notice: 'Saved search was successfully created.' }
        format.js {
          flash.now[:notice] = 'Search saved successfully.'
          render action: 'create'
        }
        format.json { render :show, status: :created, location: @saved_search }
      else
        format.html { render :new }
        format.json { render json: @saved_search.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /saved_searches/1
  # PATCH/PUT /saved_searches/1.json
  def update
    respond_to do |format|
      if @saved_search.update(saved_search_params)
        format.html { redirect_to [:user, @saved_search], notice: 'Saved search was successfully updated.' }
        format.json { render :show, status: :ok, location: @saved_search }
      else
        format.html { render :edit }
        format.json { render json: @saved_search.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /saved_searches/1
  # DELETE /saved_searches/1.json
  def destroy
    @saved_search.destroy
    respond_to do |format|
      format.html { redirect_to user_saved_searches_url, notice: 'Saved search was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_saved_search
      id = SavedSearch.dehash_id(params[:id])
      @saved_search = SavedSearch.where(:id => id, :user => @current_user).first!
      @saved_search ||= @current_user.saved_searches.build
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def saved_search_params
      params.require(:saved_search).permit!
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def search_params
      params.require(:save_search).permit!
    end
end
