class ApiAuthenticationController < DeviseController
  skip_before_action :verify_authenticity_token
  prepend_before_action :require_no_authentication, :only => [:create]
  include Devise::Controllers::Helpers

  before_action :ensure_params_exist

  respond_to :json

  def create
    build_resource
    resource = User.find_for_database_authentication(:email => params[:email])
    return invalid_login_attempt unless resource

    if resource.valid_password?(params[:password])
      render :json => {:success => true, :auth_token => resource.auth_token, :username => resource.username}
      return
    end

    invalid_login_attempt
  end

  def destroy
    sign_out(resource_name)
  end

  protected
  def ensure_params_exist
    puts params
    return unless params[:email].blank? || params[:password].blank?
    render :json => {:success => false, :message => "missing user_login parameter"}, :status => 422
  end

  def invalid_login_attempt
    warden.custom_failure!
    render :json => {:success => false, :message => "Error with your login or password"}, :status => 401
  end
end
