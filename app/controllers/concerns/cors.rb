module Cors
  extend ActiveSupport::Concern

  def cors_preflight_check
    if request.method == 'OPTIONS'
      cors_headers
      headers['Access-Control-Allow-Headers'] = request.headers['Access-Control-Request-Headers'] || ''

      render_200
    end
  end

  # For all responses in this controller, return the CORS access control headers.
  def cors_headers
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Allow-Methods'] = 'DELETE, GET, HEAD, PATCH, PUT, POST, OPTIONS'
    headers['Access-Control-Max-Age'] = '1728000'
  end

  def render_200
    head 200, content_type: 'application/json'
  end
end