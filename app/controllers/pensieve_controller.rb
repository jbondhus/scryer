class PensieveController < ApplicationController
    include Cors
  skip_before_action :verify_authenticity_token
  before_action :cors_preflight_check
  before_action :authenticate
  before_action :check_auth!
  after_action :cors_headers

  def bookmark
    if is_page_load?
      ChapterLoadEvent.from_params(current_user, request, params).save!
    else
      ChapterExitEvent.from_params(current_user, request, params).save!
    end

    render_200
  end

  protected

  def is_page_load?
    params['event_name'] == 'chapter_load'
  end

  # If this is a preflight OPTIONS request, then short-circuit the
  # request, return only the necessary headers and return an empty
  # text/plain.
  def authenticate
    authenticate_or_request_with_http_token do |token, options|
      @current_user = User.find_by(auth_token: token)
    end
  end

  def check_auth!
    unless current_user
      cors_headers
      headers['Access-Control-Allow-Headers'] = request.headers['Access-Control-Request-Headers'] || ''
      head 401, content_type: 'application/json'
    end
  end
end
