class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :authorize_profiler
  before_action :maintenance

  before_action :configure_permitted_parameters, if: :devise_controller?

  def admin?
    user_signed_in? && current_user.admin?
  end

  protected


  def authorize_profiler
    Rack::MiniProfiler.authorize_request if admin?
  end

  def maintenance
    render 'shared/maintenance', :status => 503 if Rails.configuration.maintenance && !admin?
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username])
  end
end
