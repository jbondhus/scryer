require 'hashie/mash'
require 'scryer/constants'
require 'scryer/exceptions'

class SearchController < ApplicationController

  before_action :extract_search, :except => 'report_broken'
  skip_before_action :verify_authenticity_token, :only => 'click'

  def index
    @characters = Character.for_fandoms(@fandoms)
    @fandom_facets = Fandom.fandom_facets(@fandoms)
    @tags = Tag.all.order(:id)
  end

  def omni

  end

  def report_broken
    BrokenLinkWorker.perform_async(params[:story_id].to_i)

    render status: 202, plain: 'OK'
  end

  def search
    if @search == {} || !@search
      redirect_to({ :action => :index }, alert: 'You must provide a valid search.')
      return
    end

    @page = (params[:page] || '1').to_i

    unless @search.valid?
      @characters = Character.for_fandoms(@fandoms)
      @fandom_facets = Fandom.fandom_facets(@fandoms)
      @tags = Tag.all.order(:id)
      render 'index'
      return
    end

    @search_results = @search.execute(current_user, @page, 25)
    @story_context = StoryFetchContext.new(current_user, @search_results.results.map(&:id))
    record_search
  rescue Elasticsearch::Transport::Transport::Errors::BadRequest => e
    structured_error = JSON.parse(e.message[6..-1])

    client_error_type = structured_error.dig('error', 'caused_by', 'caused_by', 'type')
    client_error_message = structured_error.dig('error', 'caused_by', 'caused_by', 'reason')

    error_string = 'Search failed: '
    if client_error_type.nil?
      error_string += 'Unknown error. :('
    else
      error_string += "#{client_error_type}: #{client_error_message}"
    end

    redirect_to(root_path(params.permit!), flash: { error: error_string})
    return
  rescue Exceptions::InvalidSearchError => e
    redirect_to(root_path(params.permit!), flash: { error: "Search failed: #{e.message}"})
    return
  end

  def click
    record_click(params)
  end

  def crossovers
    @fandom_facets = Fandom.fandom_facets(params[:fandom] || [])

    respond_to do |format|
      format.json { render json: @fandom_facets }
    end
  end

  def characters
    fandoms = (params[:fandom] || []).map(&:to_i)
    @characters = Character.for_fandoms(fandoms)

    respond_to do |format|
      format.json { render json: @characters }
    end
  end

private
  def record_search
    metadata = {ip: request.remote_ip, page: @page}
    if current_user
      metadata.merge!({user: {id: current_user.id, username: current_user.username}})
    end

    # Click.new(
    #          story_id:
    # )

    Keen.publish_async('search', @search.attributes.merge(metadata))
  rescue Keen::Error => e
    Rollbar.report_exception(e)
  end

  def record_click(event)
    story_id = event['story_id']
    story_update_id = StoryUpdate.where(story_id: story_id).select(:id).order('id DESC').first.try(:id)
    user_id = nil
    ip = request.remote_ip
    user_agent = request.user_agent
    page = event['page'].to_i

    if current_user
      user_id = current_user.id
    end

    search = event['search']
    # Keen.publish('click', event)
    # story_id, story_update_id, user_id, search, ip, page, useragent
    StoryClick.new(
                  story_id: story_id,
                  story_update_id: story_update_id,
                  user_id: user_id,
                  search: search,
                  page: page,
                  ip: ip,
                  user_agent: user_agent
    ).save

    render status: 201, plain: 'OK'
  end

  def extract_search
    @search = params[:search]
    unless @search.is_a?(ActionController::Parameters)
      @search = ActionController::Parameters.new({})
    end

    if !params[:saved_search_id].blank?
      @saved_search = SavedSearch.find(params[:saved_search_id])
    end

    if !params[:search_id].blank?
      @saved_search = SavedSearch.find(params[:search_id])

      if @saved_search
        @search = @saved_search.search
        @search_params = ActionController::Parameters.new(@search.to_hash).permit!
        @fandoms = [*@search.fandoms] + [*@search.crossovers]
        return
      end
    end

    # Some clients seem to transform search[fandoms][]=4 into search[fandoms][1]=4.
    [:fandoms, :crossovers, :rating].each do |key|
      if @search[key].is_a?(ActionController::Parameters)
        @search[key] = @search[key].values
      end
    end

    (@search[:fandoms] || []).map!(&:to_i)
    (@search[:crossovers] || []).map!(&:to_i)
    @search['crossovers'] = ([] << @search['crossovers']).flatten if @search['crossovers']

    @search_params = @search.permit!
    @search = Search.from_hash(@search.permit!)
    @fandoms = [@search['fandoms'], @search['crossovers']].flatten.compact
  end
end
