require 'jwt'

class SsoController < DeviseController
  skip_before_action :verify_authenticity_token
  prepend_before_action :require_no_authentication, :only => [:create]
  include Devise::Controllers::Helpers
  include Devise::Controllers::Rememberable
  before_action :decode_jwt

  respond_to :json

  def create
    if current_user and user.dlp_id == nil
      link_dlp_account_and_login current_user
      return
    else
      create_or_login
      return
    end
  end

  def destroy
    sign_out(resource_name)
  end

  protected
  def decode_jwt
    @jwt = (JWT.decode params[:auth_token], Rails.application.secrets.sso_key, true)[0]['message']
  rescue JWT::ExpiredSignature
    warden.custom_failure!
    redirect_to root_path, alert: 'Auth token expired.'
  end

  def create_or_login
    # Check for existing account with DLP id.
    user = User.where(dlp_id: @jwt['dlp_id']).first
    return jwt_sign_in_and_redirect(user, 'Signed in successfully.') if user


    # Check for existing account with DLP email.
    user = User.where(email: @jwt['dlp_email']).first
    return jwt_sign_in_and_redirect(link_dlp_account(user),
                                    "Your DLP account #{@jwt['dlp_username']} was linked to this account.") if user

    # Create a new user
    user = create_user_from_jwt
    jwt_sign_in_and_redirect(user, 'Thanks for creating an account!')
  end

  # Applies the DLP account id to the user.
  def link_dlp_account(user)
    user.dlp_id = @jwt['dlp_id']

    unless user.confirmed?
      # DLP verified the email.
      user.skip_confirmation!
    end

    user.save!
    user
  end

  # Signs in the user, applies the login cookie, and redirects back to root.
  def jwt_sign_in_and_redirect(user, message)
    sign_in(user, scope: :user)
    remember_me(user)
    redirect_to root_path, notice: message
  end

  # Creates a new user from the attested user with a random password. These accounts are SSO-only.
  def create_user_from_jwt
    username = @jwt['dlp_username']

    user = User.where(username: username).first
    if user
      username = "#{username}_DLP"
    end

    user = User.new(:email => @jwt['dlp_email'],
                    username: username,
                    password: SecureRandom.uuid.gsub(/\-/, ''),
                    dlp_id: @jwt['dlp_id'],
                    origin: 'sso')
    user.skip_confirmation!
    user.save!
    user
  end
end
